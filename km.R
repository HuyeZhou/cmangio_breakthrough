library(tidyverse)
source('util.R') 
df <- read_csv("data/Cox-Regression-Data-09-12-2020.csv")
df <- df %>% filter(`Delta-Time`>0) %>% 
  select(`mammo-accession`, Age, Gender, Race, `Delta-Time`, `CHD-positive`,`bac-positive`)
df <- drop_na(df)
df$`bac-positive` <- as.numeric(df$`bac-positive`)
df <- df %>% rename(BAC_group = `bac-positive`,
              censoring = `CHD-positive`,
              survival = `Delta-Time`)
surplot(Surv(survival, censoring) ~ BAC_group, df)
# df$censoring[df$survival>7.1] <- 0
# df$survival[df$survival>7.1] <- 7.1
df$age_group <- 1
df$age_group[df$Age<61] <- 0
table(df$age_group)
df$BAC_group <- 1-df$BAC_group
cox_results <- coxph(Surv(survival, censoring) ~ age_group + BAC_group, df)
cox_results_1 <- coxph(Surv(survival, censoring) ~ age_group * BAC_group, df)
j=0
c=c()
a=c()
for (i in min(df$Age):max(df$Age)){
  j=j+1
  df$age_group <- 1
  df$age_group[df$Age<i] <- 0
  a[j] <- i
  c[j] <- anova(coxph(Surv(survival, censoring) ~ age_group + BAC_group, df))[2,1]
}
plot(a,c)
summary(cox_results_1)
anova(cox_results,cox_results_1)
anova(cox_results_1)
library(rms)
df$age <- abs(df$Age-55)
ggcoxfunctional(Surv(survival, censoring) ~ age,data= df)

cox_results
summary(cox_results)
t_cox <- cox.zph(cox_results)
t_cox
ggcoxzph(t_cox)


