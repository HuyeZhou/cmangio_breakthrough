from lifelines.datasets import load_rossi
from lifelines import CoxPHFitter
import pandas as pd 
import numpy as np 
from lifelines import CoxPHFitter
from datetime import date 
import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot  as plt 

## Converting categorical values into numerical values 
def func_race(data_element):
    """
    Converting categorical labels into numbers
    White - 1 
    African-American - 2 
    Other - 3 
    """
    race = data_element['Race']
    #print(race)
    if race == 'White':
        a = 1
    elif race == 'African-American':
        a = 2
    else:
        a = 3 
    
    return a 


def func_diabeties(data_element):
    """
    Converting categorical labels into numbers
    True = 1 
    False = 0
    """
    value = data_element['History of Diabetes']
    if value == True:
        a =  1 
    else:
        a = 0
    return a 



def func_smoker(data_element):
    """
    Converting categorical labels into numbers
    Never = 1 
    Former = 2 
    Current = 3 
    """
    value = data_element['Smoker']
    if value == 'NEVER':
        a = 1
    elif value == 'FORMER':
        a = 2
    else:
        a = 3 
    
    return a 


def func_hypt(data_element):
    value = data_element['On Hypertension Treatment']
    if value == True:
        a = 1
    else:
        a = 0
    return a 

def parse_date(date):
    #print("Date recieved {}".format(date))
    if not pd.isnull(date):
        year = int(date.split('/')[2])
        month = int(date.split('/')[0])
        day = int(date.split('/')[1])
        year = 2000 + year
    else:
        pass
    return year, month, day


def rate_of_change_bac_modified(df):
    cardio_accessions = df['cardio-accession'].unique()

    cardio_BacChange = {}
    for i in range(len(cardio_accessions)):
        cardio = cardio_accessions[i]

        Bac_scores = []
        temp = df[df['cardio-accession'] == cardio]
        # Consider only the maximum BAC value if there are mulitple accessions on the same day
        dates = temp['Cardio Exam Date'].unique()
        Bac_dates = []
        for date_ in dates:
            year, month, day = parse_date(date_)
            date1 = date(year, month, day)
            temp2 = temp[temp['Cardio Exam Date'] == date_]
            bac_value = max(temp2['Bradley-Score'].values)
            Bac_scores.append(bac_value)
            Bac_dates.append(date1)
        
        max_bac_score_index = np.argmax(Bac_scores)
        max_bac_score_date = Bac_dates[max_bac_score_index]
        max_bac_score = Bac_scores[max_bac_score_index]
        earliest = np.argmin(Bac_dates)
        #oldest = np.argmax(Bac_dates)

        earliest_date = Bac_dates[earliest]
        #oldest_data = Bac_dates[oldest]
        if np.any(temp['Bradley-Score'].values) > 0:
            if earliest_date == max_bac_score_date:
                cardio_BacChange[cardio] = 0
            else:
                if len(Bac_dates)  <= 1:
                    cardio_BacChange[cardio] = 0
                else:
                    print("BAC dates :{}".format(Bac_dates))
                    print("BAC scores :{}".format(Bac_scores))
                    time_diff = max_bac_score_date - earliest_date
                    print(time_diff.days)
                    print("BAC score diff : {}".format(max_bac_score - Bac_scores[earliest]))
                    diffBAC = (max_bac_score - Bac_scores[earliest]) / (float(time_diff.days)/365.0)
                    print("Diff BAC :{}".format(diffBAC))
                    cardio_BacChange[cardio] = diffBAC
        else:
            cardio_BacChange[cardio] = 0

    return cardio_BacChange

def rate_of_change_bac(df):
    cardio_accessions = df['cardio-accession'].unique()

    cardio_BacChange = {}
    for i in range(len(cardio_accessions)):
        cardio = cardio_accessions[i]

        Bac_scores = []
        temp = df[df['cardio-accession'] == cardio]
        # Consider only the maximum BAC value if there are mulitple accessions on the same day
        dates = temp['Cardio Exam Date'].unique()
        Bac_dates = []
        for date_ in dates:
            year, month, day = parse_date(date_)
            date1 = date(year, month, day)
            temp2 = temp[temp['Cardio Exam Date'] == date_]
            bac_value = max(temp2['Bradley-Score'].values)
            Bac_scores.append(bac_value)
            Bac_dates.append(date1)
        
        earliest = np.argmin(Bac_dates)
        oldest = np.argmax(Bac_dates)

        earliest_date = Bac_dates[earliest]
        oldest_data = Bac_dates[oldest]

        if len(Bac_dates)  <= 1:
            cardio_BacChange[cardio] = 0
        else:
            print("BAC dates :{}".format(Bac_dates))
            print("BAC scores :{}".format(Bac_scores))
            time_diff = oldest_data - earliest_date
            print(time_diff.days)
            print("BAC score diff : {}".format(Bac_scores[oldest] - Bac_scores[earliest]))
            diffBAC = (Bac_scores[oldest] - Bac_scores[earliest]) / (float(time_diff.days)/365.0)
            print("Diff BAC :{}".format(diffBAC))
            cardio_BacChange[cardio] = diffBAC
    
    return cardio_BacChange

# Censor the data 
def censor(data_element):
    dt = data_element['Delta-Time-Event']
    if (dt > t_max):
        return t_max
    else:
        return dt 

def bac(data_element, threshold):
    value = data_element['Bradley-Score']
    if value > threshold :
        return 1
    else:
        return 0

def bac_As_negative(data_element):
    value = data_element['bac-positive']
    if value == True:
        return False
    else:
        return True
    
def chd_negative(data_element):
    value = data_element['CHD-positive']
    if value == 1:
        return 0
    else:
        return 1

def bac_rate(data_element, cardio_BacChange):
    cardio_accession = data_element['cardio-accession']
    rate_bac = cardio_BacChange[cardio_accession]
    return rate_bac


def normalize_bac(data_element):
    value = data_element['Bradley-Score']
    if value <= 0:
        return 0
    else:
        return value



if __name__ == "__main__":

    df = pd.read_csv('/Users/anirudh/Desktop/cmAngio_2017/Clinical_Data/Cox-Regression-Data-Event-Def-27-01-2021.csv')
    #df = pd.read_csv('/Users/anirudh/Desktop/Cox-Regression-Data-25-11-2020-final-copy.csv')
    df = df.drop(columns=['Unnamed: 0', 'mammo-accession', 'Cardio Exam Date' ,\
        'Gender', 'Start-time', 'End-time', 'First-Medical-Record-Date', 'agg_diff',\
            'Last-Medical-Record-Date', 'cac-positive', 'mammo-date-time','cad-positive', 'ASCVD-positive'])

    #df1 = pd.read_csv('/Users/anirudh/Desktop/cmAngio_2017/Clinical_Data/UCSD-correct-date-22-11-2020.csv')
    #df1['Bradley-Score'] = df1.apply(lambda row:normalize_bac(row), axis=1)
    #cardio_BacChange = rate_of_change_bac(df1)

    #cardio_BacChange1 = rate_of_change_bac_modified(df1)
    # print(cardio_BacChange)

    #dfbac_binary = df.drop(columns=['Bradley-Score'])
    #dfbac_score = df.drop(columns=['bac-positive'])

    # print(dfbac_binary.columns)
    # print(dfbac_score.columns)

    x  = df.copy()# Select the pandas data frame that you want to identify 
    x = x[x['Age'] < 65]
    # Consider only positive delta time values 
    x = x[x['Delta-Time-Event'] > 0] 

    found_valid_studies = np.array(np.where(x['Delta-Time-Event'] > 0)).flatten()
    print ('len(cmangio)        = ',len(x['Age']))
    print ('number of valid studies = ',len(x))
    # print ('max  delta t = ',np.amax(x.loc[found_valid_studies,'Delta-Time-Event'].values))
    # print ('min  delta t = ',np.amin(x.loc[found_valid_studies,'Delta-Time-Event'].values))
    # print ('mean delta t = ',np.mean(x.loc[found_valid_studies,'Delta-Time-Event'].values))
    # print ('std  delta t = ',np.std(x.loc[found_valid_studies,'Delta-Time-Event'].values))

    t_max  = 8 # This is a variable can change at times

    threshold = 0
    x['bac-positive'] = x.apply(lambda row:bac(row, threshold), axis=1)
    x['Bradley-Score'] = x.apply(lambda row:normalize_bac(row), axis=1)
    x['Delta-Time-Event'] = x.apply(lambda row: censor(row), axis=1)
    #x['bac-negative'] = x.apply(lambda row: bac_As_negative(row), axis=1)
    #x['CHD-negative'] = x.apply(lambda row: chd_negative(row), axis=1)
    censored  = np.array(np.where((x['Event-positive'] == 0) & (x['Delta-Time-Event'] < t_max))).flatten()
    # n_studies_censored = len(censored)
    # print ('# of studies = ',n_studies,', # of studies censored = ',n_studies_censored)

    x['Race'] = x.apply(lambda row: func_race(row) , axis=1)
    x['History of Diabetes'] = x.apply(lambda row: func_diabeties(row) , axis=1)
    x['Smoker'] = x.apply(lambda row: func_smoker(row) , axis=1)
    x['On Hypertension Treatment'] = x.apply(lambda row: func_hypt(row) , axis=1)
    # x['Event-positive'] = x.apply(lambda row: , axis=1)
    #x['Rate-Bac-modified'] = x.apply(lambda row: bac_rate(row, cardio_BacChange1), axis=1)
    #x['Rate-Bac'] = x.apply(lambda row: bac_rate(row, cardio_BacChange), axis=1)

    #x.to_csv('/Users/anirudh/Desktop/cmAngio_2017/Clinical_Data/Rate-of-change-BAC-Cox-regression-09-12-2020.csv')

    x = x.drop(columns=['cardio-accession'])
    # Cox Regression 
    # dtype = [('label', np.bool), ('survival_time', np.float64)]
    # delta_times = x['Delta-Time'].values
    # labels = x['CHD-positive'].values

    X = x.copy()
    print(X.columns)
    X = X.drop(columns=['HDL Cholesterol (mg/dL)', 'Systolic Blood Pressure (mm Hg)', \
        'Total Cholesterol (mg/dL)', 'HDL Cholesterol (mg/dL)', 'Smoker', \
            'ASCVD Risk', 'Bradley-Score', 'Unnamed: 0.1.1', 'Race', 'History of Diabetes', 'On Hypertension Treatment',\
            'Earliest-ASCVD-Date', 'Events', 'Actual-Event-Positive','Difference-Event', 'Earliest-Event-Date'])
    #X = X.drop(columns=['Total Cholesterol (mg/dL)', , 'History of Diabetes', 'Smoker'])
    print(X.columns)
    # print(X.head(20))
    X = X.reset_index(drop=True)
    #cph = CoxPHFitter(penalizer=0.0001)
    cph = CoxPHFitter()
    cph.fit(X, duration_col='Delta-Time-Event', event_col='Event-positive', show_progress=True)
    #cph.print_summary()
    # cph1.fit_left_censoring(X, duration_col='Delta-Time', event_col='CHD-positive' )
    # cph1.print_summary()
    n_samples, n_features = X.shape
    # print(n_samples)
    # print(n_features)

    cph.print_summary()
    # print(cph.baseline_cumulative_hazard_)
    # print(cph.params_)
    #plt.plot(cph.baseline_cumulative_hazard_)
    cph.plot()
    plt.title('Considering Positive Delta-T-values')
    plt.show()
    #plt.savefig('/Users/anirudh/Desktop/cmAngio_2017/Clinical_Data/KP-Curves-2-12-2020/Age-group-greater-than-65.png')
    # cph.plot_partial_effects_on_outcome(covariates='bac-positive', values=[0, 1], cmap='coolwarm')
    # cph.check_assumptions(X, p_value_threshold=0.01, show_plots=True)



