# This code computes the odds-ratio for BAC using Cochran-Mantel-Haenzel Estimate 
import pandas as pd 
import numpy as np 

def decide_experiment(df, Experiment):

    if Experiment == 1: 
        experiment = 'Age group 40 - 50'
        df = df[df['Age'] >= 40]
        df = df[df['Age'] < 50]
    elif Experiment == 2: 
        experiment = 'Age group 50 - 60'
        df = df[df['Age'] >= 50]
        df = df[df['Age'] < 60]
    elif Experiment == 3: 
        experiment = 'Age group 60 - 70'
        df = df[df['Age'] >= 60]
        df = df[df['Age'] < 70]
    elif Experiment == 4: 
        experiment = 'Age group 70 - 80'
        df = df[df['Age'] >= 70]
        df = df[df['Age'] < 80]
    elif Experiment == 5: 
        experiment = 'Age group 80 - 90'
        df = df[df['Age'] >= 80]
        df = df[df['Age'] < 90]
    elif Experiment == 6: 
        experiment = 'Age group 90 - 100'
        df = df[df['Age'] >= 90]
        df = df[df['Age'] < 100]
    else:
        experiment = 'All inclusive'
        df = df 

    return df, experiment 

def decide_experiment2(df, Experiment):
    if Experiment == 0:
        experiment ='Age group < 55'
        df = df[df['Age'] < 55]
    elif Experiment == 1: 
        experiment = 'Age group 55 - 65'
        df = df[df['Age'] >= 55]
        df = df[df['Age'] < 65]
    else:
        experiment = 'Age group > 65'
        df = df[df['Age'] > 65]
    
    return df, experiment 


def compute_TF_statistics(df, feature_name, feature_name_id, threshold):
    """
    df : DataFrame 
    feature_name :  CAC, BAC, ASCVD
    feature_name_id : mammo-accession, cardio-accession 
    """

    # True Positive 
    df_ASCVDTP = df[df[feature_name] == True]
    TF = df_ASCVDTP[df_ASCVDTP['Bradley-Score'] > threshold]

    True_Positive = len(TF[feature_name_id].unique())

    # False Postive 
    df_ASCVDFP = df[df[feature_name] == False]
    FP = df_ASCVDFP[df_ASCVDFP['Bradley-Score'] > threshold]

    False_Positive = len(FP[feature_name_id].unique())

    # True negative 
    df_ASCVDTN = df[df[feature_name] == False]
    TN = df_ASCVDTN[df_ASCVDTN['Bradley-Score'] <= threshold]

    True_Negative= len(TN[feature_name_id].unique())

    # False negative 
    df_ASCVDFN = df[df[feature_name] == True]
    FN = df_ASCVDFN[df_ASCVDFN['Bradley-Score'] <= threshold]

    False_Negative = len(FN[feature_name_id].unique())

    return True_Positive, False_Positive, True_Negative, False_Negative 


def compute_statistics(True_Positive, False_Positive, True_Negative, False_Negative ):
    """
    Computing only the 95% confidence interval
    z-score = 1.96
    """
    m1 = True_Positive + False_Negative
    m2 = False_Positive + True_Negative

    n1 = True_Positive + False_Positive
    n2 = False_Negative + True_Negative
    zscore = 1.96

    Sensitivity = round(True_Positive / (True_Positive + False_Negative),4)
    try:
        std_sensitivity = np.sqrt( (Sensitivity * (1 - Sensitivity) )/m1)
        CI_lower_sensitivity = round(Sensitivity - zscore*std_sensitivity,4)
        CI_upper_sensitivity = round(Sensitivity + zscore*std_sensitivity,4)
        z = np.abs(Sensitivity/std_sensitivity)
        p_value = round(np.exp(-0.717*z - 0.416*(z**2)), 5)
        sens = [Sensitivity , CI_lower_sensitivity, CI_upper_sensitivity, p_value]
        
    except:
        #Sensitivity = 0 
        sens = [Sensitivity, 0, 0, 1]

    Specificty = round(True_Negative/ (False_Positive + True_Negative),4)
    try:
        std_specificity = np.sqrt((Specificty * (1 - Specificty))/m2)
        CI_lower_specificity = round(Specificty - zscore*std_specificity, 4)
        CI_upper_specificity = round(Specificty + zscore*std_specificity, 4)
        z = np.abs(Specificty/std_specificity)
        p_value = round(np.exp(-0.717*z - 0.416*(z**2)), 5)
        spec = [Specificty, CI_lower_specificity, CI_upper_specificity, p_value]
    except:
        #Specificty = 0
        spec = [Specificty, 0, 0, 1]
    
    
    try:
        LR_positive = round(Sensitivity / (1 - Specificty), 4)
        std_lr_positive = np.sqrt((1/True_Positive) - (1/m1) + (1/False_Positive) - (1/m2))
        CI_lower_LR_positive = round(np.exp(np.log(LR_positive) - zscore*std_lr_positive),4)
        CI_upper_LR_positive = round(np.exp(np.log(LR_positive) + zscore*std_lr_positive),4)
        z = np.abs(np.log(LR_positive)/std_lr_positive)
        p_value = round(np.exp(-0.717*z - 0.416*(z**2)), 5)
        lr_pos = [LR_positive, CI_lower_LR_positive, CI_upper_LR_positive, p_value]
    except:
        LR_positive = 0
        lr_pos = [LR_positive, 0, 0, 1]
    
    
    try:
        LR_negative = round((1 - Sensitivity)/ Specificty, 4)
        std_lr_negative = np.sqrt((1/False_Negative) - (1/m1) + (1/True_Negative) - (1/m2))
        CI_lower_LR_positive = round(np.exp(np.log(LR_negative) - zscore*std_lr_negative),4)
        CI_upper_LR_positive = round(np.exp(np.log(LR_negative) + zscore*std_lr_negative),4)
        z = np.abs(np.log(LR_negative)/std_lr_negative)
        p_value = round(np.exp(-0.717*z - 0.416*(z**2)), 5)
        lr_neg = [LR_negative, CI_lower_LR_positive, CI_upper_LR_positive, p_value]
    except:
        LR_negative = 0
        lr_neg = [LR_negative, 0, 0, 1]

    try:
        Diagnostic_odds_ratios = round(LR_positive/LR_negative,4)
        std_diagnostic_odds = np.sqrt((1/True_Positive) + (1/False_Positive) + (1/False_Negative) + (1/True_Negative))
        CI_lower_diagnostic_ratio = round(np.exp(np.log(Diagnostic_odds_ratios) - zscore*std_diagnostic_odds),4)
        CI_upper_diagnostic_ratio = round(np.exp(np.log(Diagnostic_odds_ratios) + zscore*std_diagnostic_odds),4)
        z = np.abs(np.log(Diagnostic_odds_ratios)/std_diagnostic_odds)
        p_value = round(np.exp(-0.717*z - 0.416*(z**2)), 5)
        diag_odds = [Diagnostic_odds_ratios, CI_lower_diagnostic_ratio, CI_upper_diagnostic_ratio, p_value]
    except :
        Diagnostic_odds_ratios = 0
        diag_odds = [Diagnostic_odds_ratios, 0, 0, 1]
    
    
    try:
        NPV = round(True_Negative /(True_Negative + False_Negative), 4)
        std_NPV = np.sqrt((NPV*(1 - NPV))/n2)
        CI_lower_NPV = round(NPV - zscore*std_NPV, 4)
        CI_upper_NPV = round(NPV + zscore*std_NPV, 4)
        z = np.abs(NPV/std_NPV)
        p_value = round(np.exp(-0.717*z - 0.416*(z**2)), 5)
        npv = [NPV, CI_lower_NPV, CI_upper_NPV, p_value]
    except:
       NPV = 0
       npv = [NPV, 0, 0, 1]
    
    
    try:
        PPV = round(True_Positive/(True_Positive + False_Positive), 4)
        std_ppv = np.sqrt((PPV*(1-PPV))/n1)
        CI_lower_PPV = round(PPV - zscore*std_ppv, 4)
        CI_upper_PPV = round(PPV + zscore*std_ppv, 4)
        z = np.abs(PPV/std_ppv)
        p_value = round(np.exp(-0.717*z - 0.416*(z**2)), 5)
        ppv = [PPV, CI_lower_PPV, CI_upper_PPV, p_value]
    except :
        PPV = 0
        ppv = [PPV, 0, 0, 1]

    return sens ,spec, lr_pos, lr_neg, diag_odds, npv, ppv


def chi_square(TP, FP, TN, FN):
    A = TP
    ni = TP + FP + TN + FN 
    n1 = TP + FN 
    n2 = FP + TN
    m1 = TP + FP
    m2 = FN + TN

    E_A = (n1*m1)/ni
    V_A = (n1*n2*m1*m2)/(ni**2)*(ni-1)

    return A, E_A, V_A



def CI(chi_square, adjusted_ratio):

    lower_CI = np.exp((1 - 1.96/np.sqrt(chi_square))*np.log(adjusted_ratio))
    upper_CI = np.exp((1 + 1.96/np.sqrt(chi_square))*np.log(adjusted_ratio))
    return lower_CI, upper_CI

def Robins_CI(TP, FP, TN ,FN):
    # z_score = 1.96 for 95 % CI
    ni = TP + FP + TN + FN 
    Pi = (TP + TN)/ni
    Qi = (FN + FP)/ni
    Ri = (TP*TN)/ni
    Si = (FP*FN)/ni

    PR = Pi*Ri
    QS = Qi*Si
    PS_QR = Pi*Si + Qi*Ri

    return PR, PS_QR, QS, Ri, Si


if __name__ == "__main__":

    #df = pd.read_csv('/Users/anirudh/Desktop/cmAngio_2017/Clinical_Data/UCSD-ASCVD-FalseSBP-DateAdjusted-18-11-2020.csv')
    #df = pd.read_csv('Before-CHD-Mammo-Accession-Latest-accession-for-CHD-ve-24-11-2020.csv')
    #df = pd.read_csv('Before-After-CHD-Mammo-Accession-Latest-accession-for-CHD-ve-24-11-2020.csv')
    #df = pd.read_csv('Before-After-1year-CHD-Mammo-Accession-Latest-accession-for-CHD-ve-24-11-2020.csv')
    df = pd.read_csv('Before-After-1year-Atherosclerotic-Mammo-Accession-Latest-accession-for-CHD-ve-28-01-2021.csv')
    #df = pd.read_csv('BAC-prevalance-3-12-2020.csv')

    df_original = df.copy()

    df_original = df_original[df_original['agg_diff'] == 0] 

    def func(data_element):
        if (data_element['Actual-ASCVD-Positive'] == 'True'):
            return  True
        # elif (data_element['Actual-ASCVD-Positive'] == 'DK'):
        #     return True
        else:
            return False

    #df_original['Actual-ASCVD-Positive'] = df_original.apply(lambda row:func(row),axis=1)

    feature_name = 'Actual-Atherosclerotic-Positive'
    feature_name_id = 'cardio-accession'
    #threshold = 0
    #Numer = 0
    #Denom = 0
    #adjusted_ratio = 0 
    #Ai = E_Ai = V_Ai = 0
    for thres in range(0,1200,10):
        PR = PS_QR = QS = R = S = 0
        # for thres in range(0,100,10)
        record = {}
        record['BAC threshold'] = thres
        for i in range(0,7):
            df, experiment = decide_experiment(df_original, i)
            TP, FP, TN, FN = compute_TF_statistics(df, feature_name, feature_name_id, threshold=thres)
            values = [TP, FP, TN, FN]
            print("Values :{}".format(values))
            if any(v == 0 for v in values):
                TP = TP + 0.5 
                FP = FP + 0.5
                TN = TN + 0.5
                FN = FN + 0.5
            
            sens, spec, lr_pos, lr_neg, diag_odds, npv, ppv = compute_statistics(TP, FP, TN, FN)
            
            #Numer = Numer + (TP*TN)/(TP+TN+FP+FN)
            #Denom = Denom + (FP*FN)/(TP+TN+FP+FN)
            #print("Odds ratio for {}, is :{}".format(experiment, (Numer/Denom)))
            # A, E_A, V_A = chi_square(TP, FP, TN, FN)
            # Ai = A + Ai
            # E_Ai = E_Ai + E_A
            # V_Ai = V_Ai + V_A
            PRi, PS_QRi, QSi, Ri, Si  = Robins_CI(TP, FP, TN ,FN)
            PR = PR + PRi
            PS_QR = PS_QR + PS_QRi
            QS = QS + QSi
            R = R + Ri
            S = S + Si
            Age_wise_odds_ratio = np.round((Ri/Si), 4)
            record[experiment] = 'TP:{}, FP:{}, TN:{}, FN:{}'.format(TP, FP, TN, FN)
            record[experiment + 'Odds Ratio'] = '{}, ({} ,{})'.format(diag_odds[0], diag_odds[1], diag_odds[2])
            #print("New method odds ratio :{}".format(np.round((Ri/Si),4)))
            #record[experiment + 'Odds Ratio-new'] = '{}'.format(np.round((Ri/Si),4))
        #V_US = (PR/2*(R**2)) + (PS_QR/2*(R)*(S))  + (QS/2*(S))

        print("R: {}".format(R))
        print("S: {}".format(S))
        adjusted_ratio = np.round((R/S), 4)
        record['Adjusted-Odds-Ratio'] = adjusted_ratio
        # chi_square = (np.abs(Ai - E_Ai) - 0.5)**2 / V_Ai
        # lower_CI, upper_CI = CI(chi_square, adjusted_ratio)

        #lower_CI = np.exp(np.log(adjusted_ratio) - 1.96*np.sqrt(V_US))
        #upper_CI = np.exp(np.log(adjusted_ratio) + 1.96*np.sqrt(V_US))
        
        print("Adjusted Ratio Attempt 1 :{}".format(adjusted_ratio))
        #print("chi-square :{}".format(chi_square))
        #print("Confidence Intervals, lower: {}, upper: {}".format(lower_CI, upper_CI))
        print(record)
        dataframe = pd.DataFrame([record])
        if thres == 0:
            frame = dataframe
        else:
            temp = [frame, dataframe]
            frame = pd.concat(temp)


    frame.to_csv('Ajusted-odds-BAC-Atherosclerotic-before-after-1year-mammo-28-01-2021.csv')


    # feature_name = 'Actual-CHD-Positive'
    # feature_name_id = 'cardio-accession'
    # #threshold = 0
    # #Numer = 0
    # #Denom = 0
    # #adjusted_ratio = 0 
    # #Ai = E_Ai = V_Ai = 0
    # for thres in range(0,1200,10):
    #     PR = PS_QR = QS = R = S = 0
    #     # for thres in range(0,100,10)
    #     record = {}
    #     record['BAC threshold'] = thres
    #     for i in range(0,3):
    #         df, experiment = decide_experiment2(df_original, i)
    #         TP, FP, TN, FN = compute_TF_statistics(df, feature_name, feature_name_id, threshold=thres)
    #         values = [TP, FP, TN, FN]
    #         print("Values :{}".format(values))
    #         if any(v == 0 for v in values):
    #             TP = TP + 0.5 
    #             FP = FP + 0.5
    #             TN = TN + 0.5
    #             FN = FN + 0.5
            
    #         sens, spec, lr_pos, lr_neg, diag_odds, npv, ppv = compute_statistics(TP, FP, TN, FN)
            
    #         #Numer = Numer + (TP*TN)/(TP+TN+FP+FN)
    #         #Denom = Denom + (FP*FN)/(TP+TN+FP+FN)
    #         #print("Odds ratio for {}, is :{}".format(experiment, (Numer/Denom)))
    #         # A, E_A, V_A = chi_square(TP, FP, TN, FN)
    #         # Ai = A + Ai
    #         # E_Ai = E_Ai + E_A
    #         # V_Ai = V_Ai + V_A
    #         PRi, PS_QRi, QSi, Ri, Si  = Robins_CI(TP, FP, TN ,FN)
    #         PR = PR + PRi
    #         PS_QR = PS_QR + PS_QRi
    #         QS = QS + QSi
    #         R = R + Ri
    #         S = S + Si
    #         Age_wise_odds_ratio = np.round((Ri/Si), 4)
    #         record[experiment] = 'TP:{}, FP:{}, TN:{}, FN:{}'.format(TP, FP, TN, FN)
    #         record[experiment + 'Odds Ratio'] = '{}, ({} ,{})'.format(diag_odds[0], diag_odds[1], diag_odds[2])
    #         #print("New method odds ratio :{}".format(np.round((Ri/Si),4)))
    #         #record[experiment + 'Odds Ratio-new'] = '{}'.format(np.round((Ri/Si),4))
    #     #V_US = (PR/2*(R**2)) + (PS_QR/2*(R)*(S))  + (QS/2*(S))

    #     print("R: {}".format(R))
    #     print("S: {}".format(S))
    #     adjusted_ratio = np.round((R/S), 4)
    #     record['Adjusted-Odds-Ratio'] = adjusted_ratio
    #     # chi_square = (np.abs(Ai - E_Ai) - 0.5)**2 / V_Ai
    #     # lower_CI, upper_CI = CI(chi_square, adjusted_ratio)

    #     #lower_CI = np.exp(np.log(adjusted_ratio) - 1.96*np.sqrt(V_US))
    #     #upper_CI = np.exp(np.log(adjusted_ratio) + 1.96*np.sqrt(V_US))
        

    #     print("Adjusted Ratio Attempt 1 :{}".format(adjusted_ratio))
    #     #print("chi-square :{}".format(chi_square))
    #     #print("Confidence Intervals, lower: {}, upper: {}".format(lower_CI, upper_CI))
    #     print(record)
    #     dataframe = pd.DataFrame([record])
    #     if thres == 0:
    #         frame = dataframe
    #     else:
    #         temp = [frame, dataframe]
    #         frame = pd.concat(temp)


    # frame.to_csv('Ajusted-odds-bac-Mammo-CHD-latestmammo-vicinity-diff-age-group-3-12-2020.csv')
