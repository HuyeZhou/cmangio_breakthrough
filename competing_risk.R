library(tidyverse)
source('util.R') 
library(gtsummary)
library(cmprsk)
library(scales)

df_CHD <- read_csv("data/Cox-Regression-Data-09-12-2020.csv")
df_CHD_plus <- read_csv("data/Cox-Regression-Data-Event-Def-27-01-2021.csv")
df_Atherosclerosis <- read_csv("data/Cox-Regression-Data-Atherosclerotic-Events-28-01-2021.csv")

# power analysis
a <- df_CHD %>% filter(`Delta-Time`>0)
table(a$`bac-positive`)
table(a$`CHD-positive`)
coxph(Surv(`Delta-Time`, `CHD-positive`) ~ `bac-positive`, data = a) %>% 
  gtsummary::tbl_regression(exp = TRUE) 
survival.power(0.05, 124, 0.3633, 1.72)

# CHD + HF + Stroke
df_CHD_plus <- df_CHD_plus %>%
  select(`mammo-accession`, `Delta-Time-Event`, `Event-positive`, `bac-positive`) %>%
  rename(`Delta-Time` = `Delta-Time-Event`,
         `CHD-positive` = `Event-positive`)

df_CHD <- df_CHD %>% 
  select(`mammo-accession`, `Delta-Time`, `CHD-positive`, `bac-positive`)

CHD_cases <- inner_join(df_CHD_plus, df_CHD)

HF_stroke_cases <- anti_join(df_CHD_plus, CHD_cases) %>%
  mutate(`CHD-positive`=replace(`CHD-positive`, `CHD-positive`==1, 2))

df_CHD_plus <- rbind(CHD_cases, HF_stroke_cases) %>% filter(`Delta-Time`>0)
df_CHD_plus$`bac-positive` <- as.numeric(df_CHD_plus$`bac-positive`)

ci_fit <- 
  cuminc(
    ftime = df_CHD_plus$`Delta-Time`, 
    fstatus = df_CHD_plus$`CHD-positive`, 
    cencode = 0,
    group = df_CHD_plus$`bac-positive`,
  )
# plot(ci_fit)
# ggcompetingrisks(ci_fit,
#                  multiple_panels = FALSE,
#                  xlab = "Years",
#                  ylab = "Cumulative incidence of event"
#                  ) +
#   scale_y_continuous(labels = percent)

ciplotdat <- 
  ci_fit %>% 
  list_modify("Tests" = NULL) %>% 
  map_df(`[`, c("time", "est"), .id = "id") %>% 
  mutate(id = recode(
    id, 
    "0 1" = "Non-BAC:CHD", 
    "0 2" = "Non-BAC:HF+stroke", 
    "1 1" = "BAC:CHD",
    "1 2" = "BAC:HF+stroke")
  ) %>% 
  separate(id, c("BAC", "Event"), ":") 

ggplot(ciplotdat, aes(x = time, y = est, color = BAC)) +
  geom_step(aes(linetype = Event))  +
  ylim(c(0, 0.13)) +
  theme_classic() +
  theme(legend.title = element_blank(),
        legend.position = "bottom") +
  labs(x = "Years", 
       y = "Cumulative incidence",
       title = "Competing risk analysis for CHD+HF+stroke by BAC") +
  annotate("text", x = 0.5, y = 0.12, hjust = 0,
           label = paste0(
             "CHD: p = ", 
             ifelse(ci_fit$Tests[1, 2] < .001, 
                    "<.001", 
                    round(ci_fit$Tests[1, 2], 3)))) + 
  annotate("text", x = 0.5, y = 0.12-0.01, hjust = 0,
           label = paste0(
             "HF+stroke: p = ", 
             ifelse(ci_fit$Tests[2, 2] < .001, 
                    "<.001", 
                    round(ci_fit$Tests[2, 2], 3))))
















# processing here
df_CHD <- read_csv("data/Cox-Regression-Data-09-12-2020.csv")
df_CHD_plus <- read_csv("data/Cox-Regression-Data-Event-Def-27-01-2021.csv")
df_Atherosclerosis <- read_csv("data/Cox-Regression-Data-Atherosclerotic-Events-28-01-2021.csv")
df_CHD_plus <- df_CHD_plus %>%
  select(`mammo-accession`, `Delta-Time-Event`, `Event-positive`, `bac-positive`) %>%
  rename(`Delta-Time` = `Delta-Time-Event`,
         `Event` = `Event-positive`)

df_CHD <- df_CHD %>% 
  select(`mammo-accession`, `Delta-Time`, `CHD-positive`, `bac-positive`) %>% 
  rename(`Event` = `CHD-positive`)

df_Atherosclerosis <- df_Atherosclerosis %>% 
  select(`mammo-accession`, `Delta-Time-Atherosclerotic-Events`, `Atherosclerotic-Events-positive`,`bac-positive`) %>% 
  rename(`Delta-Time` = `Delta-Time-Atherosclerotic-Events`,
         `Event` = `Atherosclerotic-Events-positive`)


CHD_cases <- inner_join(df_CHD_plus, df_CHD)

HF_stroke_cases <- anti_join(df_CHD_plus, CHD_cases)

df_CHD$`event-type` <- "CHD"
HF_stroke_cases$`event-type` <- "HF+stroke"
df_Atherosclerosis$`event-type` <- "Atherosclerosis"

df_processed <- rbind(df_CHD,HF_stroke_cases, df_Atherosclerosis)

df_time <- df_processed %>% select(-Event) %>% spread(`event-type`, `Delta-Time`)
df_event <- df_processed %>% select(-`Delta-Time`) %>% spread(`event-type`, Event)

names(df_event)[3:5] <- c("Atherosclerosis-event", "CHD-event", "HF+stroke-event")
df_processed <- merge(df_time,df_event)


# CHD + Atherosclerosis

tmp <- df_processed %>% select(-`HF+stroke`, -`HF+stroke-event`)
tmp[is.na(tmp)] <- 100
tmp <-
  tmp %>%
  mutate(`Atherosclerosis-event` =
           replace(`Atherosclerosis-event`, `Atherosclerosis-event` == 1, 2)) %>%
  mutate(
    event = case_when(
      CHD <= Atherosclerosis ~ `CHD-event`,
      Atherosclerosis < CHD ~ `Atherosclerosis-event`
    )
  ) %>%
  mutate(time = case_when(
    CHD <= Atherosclerosis ~ CHD,
    Atherosclerosis < CHD ~ Atherosclerosis
  )) %>%
  filter(time>0 & time<100)
tmp$`bac-positive` <- as.numeric(tmp$`bac-positive`)
ci_fit <- 
  cuminc(
    ftime = tmp$time, 
    fstatus = tmp$event, 
    cencode = 0,
    group = tmp$`bac-positive`,
  )
# plot(ci_fit)
# ggcompetingrisks(ci_fit,
#                  multiple_panels = FALSE,
#                  xlab = "Years",
#                  ylab = "Cumulative incidence of event"
#                  ) +
#   scale_y_continuous(labels = percent)

ciplotdat <- 
  ci_fit %>% 
  list_modify("Tests" = NULL) %>% 
  map_df(`[`, c("time", "est"), .id = "id") %>% 
  mutate(id = recode(
    id, 
    "0 1" = "Non-BAC:CHD", 
    "0 2" = "Non-BAC:Atherosclerosis", 
    "1 1" = "BAC:CHD",
    "1 2" = "BAC:Atherosclerosis")
  ) %>% 
  separate(id, c("BAC", "Event"), ":") 

ggplot(ciplotdat, aes(x = time, y = est, color = BAC)) +
  geom_step(aes(linetype = Event))  +
  ylim(c(0, 0.16)) +
  theme_classic() +
  theme(legend.title = element_blank(),
        legend.position = "bottom") +
  labs(x = "Years", 
       y = "Cumulative incidence",
       title = "Competing risk analysis for CHD+Atherosclerosis by BAC") +
  annotate("text", x = 0.5, y = 0.15, hjust = 0,
           label = paste0(
             "CHD: p = ", 
             ifelse(ci_fit$Tests[1, 2] < .001, 
                    "<.001", 
                    round(ci_fit$Tests[1, 2], 3)))) + 
  annotate("text", x = 0.5, y = 0.15-0.01, hjust = 0,
           label = paste0(
             "Atherosclerosis: p = ", 
             ifelse(ci_fit$Tests[2, 2] < .001, 
                    "<.001", 
                    round(ci_fit$Tests[2, 2], 3))))


# CHD + HF + stroke

tmp <- df_processed %>% select(-Atherosclerosis, -`Atherosclerosis-event`)
tmp[is.na(tmp)] <- 100
tmp <-
  tmp %>%
  mutate(`HF+stroke-event` =
           replace(`HF+stroke-event`, `HF+stroke-event` == 1, 3)) %>%
  mutate(
    event = case_when(
      CHD <= `HF+stroke` ~ `CHD-event`,
      `HF+stroke` < CHD ~ `HF+stroke-event`
    )
  ) %>%
  mutate(time = case_when(
    CHD <= `HF+stroke` ~ CHD,
    `HF+stroke` < CHD ~ `HF+stroke`
  )) %>%
  filter(time>0 & time<100)
tmp$`bac-positive` <- as.numeric(tmp$`bac-positive`)
ci_fit <- 
  cuminc(
    ftime = tmp$time, 
    fstatus = tmp$event, 
    cencode = 0,
    group = tmp$`bac-positive`,
  )
# plot(ci_fit)
# ggcompetingrisks(ci_fit,
#                  multiple_panels = FALSE,
#                  xlab = "Years",
#                  ylab = "Cumulative incidence of event"
#                  ) +
#   scale_y_continuous(labels = percent)

ciplotdat <- 
  ci_fit %>% 
  list_modify("Tests" = NULL) %>% 
  map_df(`[`, c("time", "est"), .id = "id") %>% 
  mutate(id = recode(
    id, 
    "0 1" = "Non-BAC:CHD", 
    "0 3" = "Non-BAC:HF+stroke", 
    "1 1" = "BAC:CHD",
    "1 3" = "BAC:HF+stroke")
  ) %>% 
  separate(id, c("BAC", "Event"), ":") 

ggplot(ciplotdat, aes(x = time, y = est, color = BAC)) +
  geom_step(aes(linetype = Event))  +
  ylim(c(0, 0.1)) +
  theme_classic() +
  theme(legend.title = element_blank(),
        legend.position = "bottom") +
  labs(x = "Years", 
       y = "Cumulative incidence",
       title = "Competing risk analysis for CHD+HF+stroke by BAC") +
  annotate("text", x = 0.5, y = 0.1, hjust = 0,
           label = paste0(
             "CHD: p = ", 
             ifelse(ci_fit$Tests[1, 2] < .001, 
                    "<.001", 
                    round(ci_fit$Tests[1, 2], 3)))) + 
  annotate("text", x = 0.5, y = 0.1-0.01, hjust = 0,
           label = paste0(
             "HF+stroke: p = ", 
             ifelse(ci_fit$Tests[2, 2] < .001, 
                    "<.001", 
                    round(ci_fit$Tests[2, 2], 3))))




# CHD + HF + stroke + Atherosclerosis

tmp <- df_processed
tmp[is.na(tmp)] <- 100
tmp <-
  tmp %>% 
  mutate(`Atherosclerosis-event` =
           replace(`Atherosclerosis-event`, `Atherosclerosis-event` == 1, 2)) %>%
  mutate(`HF+stroke-event` =
           replace(`HF+stroke-event`, `HF+stroke-event` == 1, 3)) %>%
  mutate(
    event = case_when(
      Atherosclerosis <= CHD & Atherosclerosis <= `HF+stroke` ~ `Atherosclerosis-event`,
      CHD <= Atherosclerosis & CHD <= `HF+stroke` ~ `CHD-event`,
      `HF+stroke` <= CHD & `HF+stroke` <= Atherosclerosis ~ `HF+stroke-event`
    )
  ) %>%
  mutate(time = case_when(
    Atherosclerosis <= CHD & Atherosclerosis <= `HF+stroke` ~ Atherosclerosis,
    CHD <= Atherosclerosis & CHD <= `HF+stroke` ~ CHD,
    `HF+stroke` <= CHD & `HF+stroke` <= Atherosclerosis ~ `HF+stroke`
  )) %>%
  filter(time>0 & time<100)
tmp$`bac-positive` <- as.numeric(tmp$`bac-positive`)
ci_fit <- 
  cuminc(
    ftime = tmp$time, 
    fstatus = tmp$event, 
    cencode = 0,
    group = tmp$`bac-positive`,
  )
# plot(ci_fit)
# ggcompetingrisks(ci_fit,
#                  multiple_panels = FALSE,
#                  xlab = "Years",
#                  ylab = "Cumulative incidence of event"
#                  ) +
#   scale_y_continuous(labels = percent)

ciplotdat <- 
  ci_fit %>% 
  list_modify("Tests" = NULL) %>% 
  map_df(`[`, c("time", "est"), .id = "id") %>% 
  mutate(id = recode(
    id, 
    "0 1" = "Non-BAC:CHD", 
    "0 2" = "Non-BAC:Atherosclerosis", 
    "0 3" = "Non-BAC:HF+stroke", 
    "1 1" = "BAC:CHD",
    "1 2" = "BAC:Atherosclerosis",
    "1 3" = "BAC:HF+stroke")
  ) %>% 
  separate(id, c("BAC", "Event"), ":") 

ggplot(ciplotdat, aes(x = time, y = est, color = BAC)) +
  geom_step(aes(linetype = Event))  +
  ylim(c(0, 0.15)) +
  theme_classic() +
  theme(legend.title = element_blank(),
        legend.position = "bottom") +
  labs(x = "Years", 
       y = "Cumulative incidence",
       title = "Competing risk analysis for CHD+HF+stroke+Atherosclerosis by BAC") +
  annotate("text", x = 0.5, y = 0.15, hjust = 0,
           label = paste0(
             "CHD: p = ", 
             ifelse(ci_fit$Tests[1, 2] < .001, 
                    "<.001", 
                    round(ci_fit$Tests[1, 2], 3)))) + 
  annotate("text", x = 0.5, y = 0.15-0.01, hjust = 0,
           label = paste0(
             "Atherosclerosis: p = ", 
             ifelse(ci_fit$Tests[2, 2] < .001, 
                    "<.001", 
                    round(ci_fit$Tests[2, 2], 3)))) + 
  annotate("text", x = 0.5, y = 0.15-0.02, hjust = 0,
           label = paste0(
             "HF+stroke: p = ", 
             ifelse(ci_fit$Tests[3, 2] < .001, 
                    "<.001", 
                    round(ci_fit$Tests[3, 2], 3))))

