library(tidyverse)
df <- read_csv("data/NEW-DATA-UCSD-OLD-DATA-09-02-2021_UPDATED.csv")
df <- df %>% rename(
  HDL_Cholesterol = `HDL Cholesterol (mg/dL)`,
  Total_Cholesterol = `Total Cholesterol (mg/dL)`,
  SBP = `Systolic Blood Pressure (mm Hg)`,
  Hypertension = `On Hypertension Treatment`,
  Diabities = `History of Diabetes`,
) %>% mutate(Gender = as.factor(Gender)) %>%
  mutate(Hypertension = as.numeric(Hypertension)) %>% 
  mutate(Diabities = as.numeric(Diabities)) %>% 
  mutate(Smoker = case_when(Smoker == "CURRENT" ~1,
                            Smoker != "CURRENT" ~0)) %>% 
  mutate(Smoker = as.factor(Smoker)) %>% 
  mutate(BAC = case_when(`Bradley-Score`>=0~`Bradley-Score`,
                         `Bradley-Score`<0~0)) %>% 
  mutate(Race = as.factor(Race))
event <- df %>% filter(`Difference-Event`>0)
event$label <- 1
non_event <- df[!(df$Events),]
non_event$label <- 0
set.seed(123)

train_event_idx <- sample(nrow(event), nrow(event)*0.7)
non_event <- non_event[sample(nrow(non_event), nrow(event)/0.065),]
train_non_event_idx <- sample(nrow(non_event), nrow(non_event)*0.7)
train <- rbind(event[train_event_idx,], non_event[train_non_event_idx,])
val <- rbind(event[-train_event_idx,], non_event[-train_non_event_idx,])
library(mgcv)
library(aod)
mod_lm <- gam(I(label) ~ Hypertension*SBP+
                Age*HDL_Cholesterol + 
                Age*Total_Cholesterol +
                Age*SBP +
                Age*Hypertension +
                Age*Race+
                Race*HDL_Cholesterol + 
                Race*Total_Cholesterol +
                BAC*HDL_Cholesterol + 
                BAC*Total_Cholesterol +
                BAC*SBP +
                BAC*Hypertension +
                BAC*Diabities +
                BAC*Smoker
                , data=train)
summary(mod_lm)
val$predict <- predict(mod_lm, newdata = val, type = "response")
train$predict <- predict(mod_lm, newdata = train, type = "response")
library(pROC)
pROC_obj <- roc(val$label,val$predict,
                smoothed = TRUE,
                # arguments for ci
                ci=TRUE, ci.alpha=0.95, stratified=FALSE,
                # arguments for plot
                plot=TRUE, auc.polygon=TRUE, max.auc.polygon=TRUE, grid=TRUE,
                print.auc=TRUE, show.thres=TRUE)
sens.ci <- ci.se(pROC_obj)
plot(sens.ci, type="shape", col="lightblue")
